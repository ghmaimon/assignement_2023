package ma.octo.assignement.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.enumeration.EventType;
import ma.octo.assignement.exceptions.AccountDoesNotExistException;
import ma.octo.assignement.exceptions.InsufficientBalanceException;
import ma.octo.assignement.exceptions.EventException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.DepositMapperImpl;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.model.Deposit;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.serviceImpl.DepositServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {
        DepositMapperImpl.class,
})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DepositServiceTest {

    private DepositRepository depositRepository;
    private ObjectMapper objectMapper;
    @Autowired
    private DepositMapper depositMapper;
    private DepositService depositService;
    private AccountService accountService;
    private AuditService auditService;
    private List<Account> accounts;

    @BeforeAll
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        this.depositRepository = mock(DepositRepository.class);
        this.accountService = mock(AccountService.class);
        this.auditService = mock(AuditService.class);
        this.depositService = new DepositServiceImpl(
                this.depositRepository,
                this.accountService,
                this.depositMapper,
                this.auditService) {
        };

        Account[] array;
        try (
                InputStream inputStream =
                        Files.newInputStream(
                                new File("src/test/resources/accounts.json")
                                        .toPath())
        ) {
            String json = new String(
                    inputStream.readAllBytes()
            );
            array = objectMapper.readValue(
                    json,
                    Account[].class
            );
            accounts = List.of(array);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testLoadAllDeposits() {

        // **** given
        Deposit[] array;
        List<Deposit> deposits;
        try (
                InputStream inputStream =
                        Files.newInputStream(
                                new File("src/test/resources/load_all_deposits_response.json")
                                        .toPath())
        ) {
            String json = new String(
                    inputStream.readAllBytes()
            );
            array = objectMapper.readValue(
                    json,
                    Deposit[].class
            );
            deposits = List.of(array);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        given(depositRepository.findAll()).willReturn(deposits);

        // ***** when
        List<Deposit> transferList = depositService.loadAllDeposits();

        // then
        assertThat(transferList.size(), is(1));
        assertThat(transferList.get(0).getReason(), is("Assignment 2021"));
        assertThat(transferList.get(0).getType(), is(EventType.DEPOSIT));
    }

    @Test
    void createDeposit_happyPath() {

        //given
        ArgumentCaptor<Deposit> argumentCaptor = ArgumentCaptor.forClass(Deposit.class);
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(0));


        CreateDepositRequestDto dto = new CreateDepositRequestDto();
        dto.setAmount(new BigDecimal(554));
        dto.setReason("anything");
        dto.setFullNameCreditor("person x");
        dto.setNumAccountDebtor("22222");

        //when
        depositService.createDeposit(dto);

        // then
        then(depositRepository).should().save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is(EventType.DEPOSIT));
    }

    @Test
    void createDeposit_accountNotFound() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willThrow(new AccountDoesNotExistException("Debtor with id account number '11111' not found"));


        CreateDepositRequestDto dto = new CreateDepositRequestDto();
        dto.setNumAccountDebtor("11111");

        //when
        AccountDoesNotExistException exception =
                assertThrows(
                        AccountDoesNotExistException.class,
                        () -> depositService.createDeposit(dto)
                );

        // then
        assertThat(exception.getMessage(), is("Account not found: Debtor with id account number '11111' not found"));
    }

    @Test
    void createDeposit_minimumAmountNotReached() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(0));


        CreateDepositRequestDto dto = new CreateDepositRequestDto();
        dto.setAmount(new BigDecimal(5));
        dto.setReason("anything");
        dto.setFullNameCreditor("person x");
        dto.setNumAccountDebtor("22222");

        //when
        EventException transferException = assertThrows(
                EventException.class,
                () -> depositService.createDeposit(dto)
        );

        // then
        assertThat(transferException.getMessage(), is("Event Exception: Minimum Amount for the transfer is not reached!"));
    }

    @Test
    void createDeposit_maximumAmountReached() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(0));

        CreateDepositRequestDto dto = new CreateDepositRequestDto();
        dto.setAmount(new BigDecimal(5000000));
        dto.setReason("anything");
        dto.setFullNameCreditor("person x");
        dto.setNumAccountDebtor("22222");

        //when
        EventException transferException = assertThrows(
                EventException.class,
                () -> depositService.createDeposit(dto)
        );

        // then
        assertThat(transferException.getMessage(), is("Event Exception: Maximum amount for the transfer is reached!"));
    }

    @Test
    void createTransfer_auditSaved() {


        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(0));

        CreateDepositRequestDto dto = new CreateDepositRequestDto();
        dto.setAmount(new BigDecimal(10000));
        dto.setReason("anything");
        dto.setFullNameCreditor("person x");
        dto.setNumAccountDebtor("22222");

        //when
        Deposit deposit = depositService.createDeposit(dto);

        // then
        then(auditService).should(times(2)).saveAudit(deposit);
    }
}
