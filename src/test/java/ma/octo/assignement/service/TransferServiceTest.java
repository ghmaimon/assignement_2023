package ma.octo.assignement.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.enumeration.EventType;
import ma.octo.assignement.exceptions.AccountDoesNotExistException;
import ma.octo.assignement.exceptions.InsufficientBalanceException;
import ma.octo.assignement.exceptions.EventException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.mapper.TransferMapperImpl;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.serviceImpl.TransferServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {
        TransferMapperImpl.class,
})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransferServiceTest {

    private TransferRepository transferRepository;
    private ObjectMapper objectMapper;
    @Autowired
    private TransferMapper transferMapper;
    private TransferService transferService;
    private AccountService accountService;
    private AuditService auditService;
    private List<Account> accounts;

    @BeforeAll
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        this.transferRepository = mock(TransferRepository.class);
        this.accountService = mock(AccountService.class);
        this.auditService = mock(AuditService.class);
        this.transferService = new TransferServiceImpl(
                this.transferRepository,
                this.accountService,
                this.auditService,
                transferMapper);

        Account[] array;
        try (
                InputStream inputStream =
                        Files.newInputStream(
                                new File("src/test/resources/accounts.json")
                                        .toPath())
        ){
            String json = new String(
                    inputStream.readAllBytes()
            );
            array = objectMapper.readValue(
                    json,
                    Account[].class
            );
            accounts = List.of(array);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testLoadAllTransfers() {

        // **** given
        Transfer[] array;
        List<Transfer> transfers;
        try (
                InputStream inputStream =
                        Files.newInputStream(
                                new File("src/test/resources/load_all_transfers_response.json")
                                        .toPath())
        ){
            String json = new String(
                   inputStream.readAllBytes()
            );
            array = objectMapper.readValue(
                    json,
                    Transfer[].class
            );
            transfers = List.of(array);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        given(transferRepository.findAll()).willReturn(transfers);

        // ***** when
        List<Transfer> transferList = transferService.loadAllTransfers();

        // then
        assertThat(transferList.size(), is(1));
        assertThat(transferList.get(0).getReason(), is("Assignment 2021"));
        assertThat(transferList.get(0).getType(), is(EventType.TRANSFER));

    }

    @Test
    void createTransfer_happyPath() {

        //given
        ArgumentCaptor<Transfer> argumentCaptor = ArgumentCaptor.forClass(Transfer.class);
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willReturn(accounts.get(0));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setAmount(new BigDecimal(554));
        dto.setReason("anything");
        dto.setNumAccountCreditor("11111");
        dto.setNumAccountDebtor("22222");

        //when
        transferService.createTransfer(dto);

        // then
        then(transferRepository).should(times(2)).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getType(), is(EventType.TRANSFER));
    }

    @Test
    void createTransfer_accountNotFound() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willThrow(new AccountDoesNotExistException("Creditor with id account number '11111' not found"));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setNumAccountCreditor("11111");

        //when
        AccountDoesNotExistException exception =
                assertThrows(
                        AccountDoesNotExistException.class,
                        () -> transferService.createTransfer(dto)
                );

        // then
        assertThat(exception.getMessage(), is("Account not found: Creditor with id account number '11111' not found"));
    }

    @Test
    void createTransfer_minimumAmountNotReached() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willReturn(accounts.get(0));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setAmount(new BigDecimal(5));
        dto.setReason("anything");
        dto.setNumAccountCreditor("11111");
        dto.setNumAccountDebtor("22222");

        //when
        EventException transferException = assertThrows(
                EventException.class,
                () -> transferService.createTransfer(dto)
        );

        // then
        assertThat(transferException.getMessage(), is("Event Exception: Minimum Amount for the transfer is not reached!"));
    }

    @Test
    void createTransfer_maximumAmountReached() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willReturn(accounts.get(0));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setAmount(new BigDecimal(5000000));
        dto.setReason("anything");
        dto.setNumAccountCreditor("11111");
        dto.setNumAccountDebtor("22222");

        //when
        EventException transferException = assertThrows(
                EventException.class,
                () -> transferService.createTransfer(dto)
        );

        // then
        assertThat(transferException.getMessage(), is("Event Exception: Maximum amount for the transfer is reached!"));
    }

    @Test
    void createTransfer_InsufficientBalance() {

        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willReturn(accounts.get(2));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setAmount(new BigDecimal(10000));
        dto.setReason("anything");
        dto.setNumAccountCreditor("11111");
        dto.setNumAccountDebtor("22222");

        //when
        InsufficientBalanceException insufficientBalanceException = assertThrows(
                InsufficientBalanceException.class,
                () -> transferService.createTransfer(dto)
        );

        // then
        assertThat(insufficientBalanceException.getMessage(), is("Insufficient balance: Creditor has insufficient balance!"));
    }

    @Test
    void createTransfer_auditSaved() {


        //given
        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.CREDITOR)
        )).willReturn(accounts.get(0));

        given(accountService.findByNumAccount(
                anyString(), eq(AccountType.DEBTOR)
        )).willReturn(accounts.get(1));

        CreateTransferRequestDto dto = new CreateTransferRequestDto();
        dto.setAmount(new BigDecimal(10000));
        dto.setReason("anything");
        dto.setNumAccountCreditor("11111");
        dto.setNumAccountDebtor("22222");

        //when
        Transfer transfer = transferService.createTransfer(dto);

        // then
        then(auditService).should().saveAudit(transfer);
    }
}