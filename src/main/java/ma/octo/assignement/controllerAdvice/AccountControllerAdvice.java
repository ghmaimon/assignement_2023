package ma.octo.assignement.controllerAdvice;

import ma.octo.assignement.exceptions.AccountDoesNotExistException;
import ma.octo.assignement.exceptions.InsufficientBalanceException;
import ma.octo.assignement.exceptions.NiceBankExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;


@ControllerAdvice
public class AccountControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InsufficientBalanceException.class)
    public ResponseEntity<NiceBankExceptionResponse> handleInsufficientBalanceException(
            InsufficientBalanceException ex,
            WebRequest request) {

        return new ResponseEntity<>(
                new NiceBankExceptionResponse(
                        ex.getMessage(),
                        Instant.now()
                ),
                HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS
        );
    }

    @ExceptionHandler(AccountDoesNotExistException.class)
    public ResponseEntity<NiceBankExceptionResponse> handleAccountDoesNotExistException(
            AccountDoesNotExistException ex,
            WebRequest request) {
        return new ResponseEntity<>(
                new NiceBankExceptionResponse(
                        ex.getMessage(),
                        Instant.now()
                ),
                HttpStatus.UNAUTHORIZED
        );
    }
}
