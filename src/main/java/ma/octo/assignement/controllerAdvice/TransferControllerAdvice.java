package ma.octo.assignement.controllerAdvice;

import ma.octo.assignement.exceptions.NiceBankExceptionResponse;
import ma.octo.assignement.exceptions.EventException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.Instant;

@ControllerAdvice
public class TransferControllerAdvice {

    @ExceptionHandler(EventException.class)
    public ResponseEntity<NiceBankExceptionResponse> handleTransferException(
            EventException ex,
            WebRequest request) {
        return new ResponseEntity<>(
                new NiceBankExceptionResponse(
                        ex.getMessage(),
                        Instant.now()
                ),
                HttpStatus.BAD_REQUEST
        );
    }

}
