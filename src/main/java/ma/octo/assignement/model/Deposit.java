package ma.octo.assignement.model;

import lombok.*;
import ma.octo.assignement.enumeration.EventType;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@DiscriminatorValue("DEPOSIT")
public class Deposit extends Event{

    {
      this.setType(EventType.DEPOSIT);
    }

    @ManyToOne
    private Account accountDebtor;

    @Column(name = "fullNameCreditor")
    private String fullNameCreditor;

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
      Deposit that = (Deposit) o;
      return id != null && Objects.equals(id, that.id);
    }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
