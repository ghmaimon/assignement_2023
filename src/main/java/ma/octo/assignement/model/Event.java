package ma.octo.assignement.model;

import lombok.*;
import ma.octo.assignement.enumeration.EventType;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "EVENT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "event_type", discriminatorType = DiscriminatorType.STRING)
@Getter
@Setter
@ToString
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    protected BigDecimal amount;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date date;

    @ManyToOne
    protected Account accountDebtor;

    @Column(length = 200)
    protected String reason;

    @Column(name="event_type", insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    protected EventType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Event event = (Event) o;
        return id != null && Objects.equals(id, event.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
