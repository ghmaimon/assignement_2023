package ma.octo.assignement.repository;

import ma.octo.assignement.model.Audit;
import ma.octo.assignement.model.AuditTransfer;
import org.springframework.data.jpa.repository.JpaRepository;

// this interface is public, so it should have the same name as the java file's name:
public interface AuditRepository extends JpaRepository<Audit, Long> {
}
