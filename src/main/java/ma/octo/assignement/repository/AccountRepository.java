package ma.octo.assignement.repository;

import ma.octo.assignement.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
  Optional<Account> findByNumAccount(String numAccount);
}
