package ma.octo.assignement.service;

import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.model.Transfer;
import java.util.List;

public interface TransferService {
    List<Transfer> loadAllTransfers();
    Transfer createTransfer(CreateTransferRequestDto dto);
}
