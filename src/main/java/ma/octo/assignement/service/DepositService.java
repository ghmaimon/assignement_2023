package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.model.Deposit;
import ma.octo.assignement.repository.DepositRepository;
import org.springframework.stereotype.Service;

import java.util.List;

public interface DepositService {
    List<Deposit> loadAllDeposits();
    Deposit createDeposit(CreateDepositRequestDto depositDto);
}
