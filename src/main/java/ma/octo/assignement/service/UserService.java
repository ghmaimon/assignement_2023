package ma.octo.assignement.service;

import ma.octo.assignement.model.User;

import java.util.List;

public interface UserService {
    List<User> loadAllUsers();
}
