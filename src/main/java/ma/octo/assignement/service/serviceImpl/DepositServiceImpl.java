package ma.octo.assignement.service.serviceImpl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.exceptions.EventException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.model.Deposit;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Primary
@Service
@AllArgsConstructor
public class DepositServiceImpl implements DepositService {

    public static final int MAXIMUM_AMOUNT = 10000;
    public static final int MINIMUM_AMOUNT = 10;
    private DepositRepository depositRepository;
    private AccountService accountService;
    private DepositMapper depositMapper;
    private AuditService auditService;

    public List<Deposit> loadAllDeposits() {
        List<Deposit> deposits = depositRepository.findAll();
        return deposits.isEmpty()? null: deposits;
    }

    public Deposit createDeposit(CreateDepositRequestDto depositDto) {

        Account accountDebtor = accountService.findByNumAccount(depositDto.getNumAccountDebtor(), AccountType.DEBTOR);

        if (depositDto.getAmount() == null)
            throw new EventException("Amount is empty!");

        else if (depositDto.getAmount().equals(BigDecimal.ZERO))
            throw new EventException("Amount equals to 0!");

        else if (depositDto.getAmount().intValue() < MINIMUM_AMOUNT)
            throw new EventException("Minimum Amount for the transfer is not reached!");

        else if (depositDto.getAmount().intValue() > MAXIMUM_AMOUNT)
            throw new EventException("Maximum amount for the transfer is reached!");

        if (depositDto.getReason() == null || depositDto.getReason().equals(""))
            throw new EventException("Reason is empty!");

        accountDebtor.setBalance(accountDebtor.getBalance().add(depositDto.getAmount()));
        accountService.updateAccount(accountDebtor);

        Deposit deposit = depositMapper.createDepositRequestDtoToDepositMapper(depositDto);

        deposit.setAccountDebtor(accountDebtor);

        if (depositDto.getDate() == null) // if there is no specific date sent in the request, we can add the current date automatically
            deposit.setDate(Date.from(Instant.now()));

        deposit = depositRepository.save(deposit);

        auditService.saveAudit(deposit);

        return deposit;
    }
}
