package ma.octo.assignement.service.serviceImpl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.model.User;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.service.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

// this service will deal with all the logic concerning users
@Service
@Primary
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    public List<User> loadAllUsers() {
        List<User> users = userRepository.findAll();
        return users.isEmpty() ? null : users;
    }
}
