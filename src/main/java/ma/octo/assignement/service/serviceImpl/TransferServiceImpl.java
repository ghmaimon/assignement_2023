package ma.octo.assignement.service.serviceImpl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.exceptions.InsufficientBalanceException;
import ma.octo.assignement.exceptions.EventException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;


// This service will deal with all the logic concerning Transfers:
@Service
@Primary
@AllArgsConstructor // this will deal with the dependency injection for us
@Transactional
@Slf4j // for logging
public class TransferServiceImpl implements TransferService {

    public static final int MAXIMUM_AMOUNT = 10000;
    public static final int MINIMUM_AMOUNT = 10;
    private TransferRepository transferRepository;
    private AccountService accountService;
    private AuditService auditService;
    private TransferMapper transferMapper;
    public List<Transfer> loadAllTransfers() {
        List<Transfer> transfers = transferRepository.findAll();
        return transfers.isEmpty() ? null : transfers;
    }

    public Transfer createTransfer(CreateTransferRequestDto dto) {

        Account accountCreditor = accountService.findByNumAccount(dto.getNumAccountCreditor(), AccountType.CREDITOR);
        Account accountDebtor = accountService.findByNumAccount(dto.getNumAccountDebtor(), AccountType.DEBTOR);

        if (dto.getAmount() == null)
            throw new EventException("Amount is empty!");

        else if (dto.getAmount().equals(BigDecimal.ZERO))
            throw new EventException("Amount equals to 0!");

        else if (dto.getAmount().intValue() < MINIMUM_AMOUNT)
            throw new EventException("Minimum Amount for the transfer is not reached!");

        else if (dto.getAmount().intValue() > MAXIMUM_AMOUNT)
            throw new EventException("Maximum amount for the transfer is reached!");

        if (dto.getReason() == null || dto.getReason().equals(""))
            throw new EventException("Reason is empty!");

        if (
                accountCreditor.getBalance().
                        subtract(dto.getAmount())
                        .compareTo(BigDecimal.ZERO) < 0
        ) throw new InsufficientBalanceException("Creditor has insufficient balance!");


        accountCreditor.setBalance(accountCreditor.getBalance().subtract(dto.getAmount()));
        accountService.updateAccount(accountCreditor);

        accountDebtor.setBalance(accountDebtor.getBalance().add(dto.getAmount()));
        accountService.updateAccount(accountDebtor);

        Transfer transfer = transferMapper.createTransferRequestDtoToTransferMapper(dto);

        transfer.setAccountDebtor(accountDebtor);
        transfer.setAccountCreditor(accountCreditor);

        if (dto.getDate() == null) // if there is no specific date sent in the request, we can add the current date automatically
            transfer.setDate(Date.from(Instant.now()));

        transfer = transferRepository.save(transfer);

        auditService.saveAudit(transfer);

        return transfer;
    }
}
