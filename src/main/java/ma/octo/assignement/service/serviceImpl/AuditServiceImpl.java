package ma.octo.assignement.service.serviceImpl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.enumeration.EventType;
import ma.octo.assignement.factory.DepositAuditFactory;
import ma.octo.assignement.factory.TransferAuditFactory;
import ma.octo.assignement.model.*;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Primary
@Transactional
@AllArgsConstructor
@Slf4j
public class AuditServiceImpl implements AuditService {

    private final AuditRepository auditRepository;
    private final TransferAuditFactory auditTransferFactory;
    private final DepositAuditFactory auditDepositFactory;

    public Audit auditTransfer(Event event) {
        Audit audit = auditTransferFactory.performAuditOperation(event);
        auditRepository.save(audit);
        return audit;
    }


    public Audit auditDeposit(Event event) {
        Audit audit = auditDepositFactory.performAuditOperation(event);
        auditRepository.save(audit);
        return audit;
    }

    public Audit saveAudit(Event event) {
        if (event.getType().equals(EventType.TRANSFER))
            return auditTransfer(event);
        else if (event.getType().equals(EventType.DEPOSIT))
            return auditDeposit(event);
        else {
            log.error("unknown event type");
            return null;
        }
    }
}
