package ma.octo.assignement.service.serviceImpl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.exceptions.AccountDoesNotExistException;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;


// this service will deal with all the logic of accounts
@Service
@Primary
@AllArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;
    public List<Account> loadAllAccounts() {
        List<Account> accounts = accountRepository.findAll();
        return accounts.isEmpty() ? null : accounts;
    }

    public Account findByNumAccount(String numAccount, AccountType type) throws AccountDoesNotExistException {
        log.info(type.getType());
        return accountRepository
                .findByNumAccount(numAccount)
                .orElseThrow(
                        () -> new AccountDoesNotExistException(
                                type.getType() + " with id account number '" + numAccount + "' not found")
                );
    }

    public void updateAccount(Account accountCreditor) {
        accountRepository.save(accountCreditor);
    }
}
