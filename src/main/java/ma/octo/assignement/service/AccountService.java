package ma.octo.assignement.service;

import ma.octo.assignement.enumeration.AccountType;
import ma.octo.assignement.exceptions.AccountDoesNotExistException;
import ma.octo.assignement.model.Account;

import java.util.List;

public interface AccountService {
    List<Account> loadAllAccounts();
    Account findByNumAccount(String numAccount, AccountType type);
    void updateAccount(Account account);
}
