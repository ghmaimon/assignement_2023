package ma.octo.assignement.service;

import ma.octo.assignement.model.Audit;
import ma.octo.assignement.model.Event;

public interface AuditService {
    Audit saveAudit(Event event);
}