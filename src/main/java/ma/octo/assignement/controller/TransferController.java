package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import ma.octo.assignement.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transfers") // the path should be specified in the @RequestMapping annotation, not in @RestController
@AllArgsConstructor //rather than hard coding a constructor, using Lombok we don't have to bear the burden of changing this constructor whenever a new dependency is added
class TransferController {

    /*
    private CompteRepository rep1;  it is bad practice when a controller deals with repositories
    it is better for repositories and controllers to communicate through services.
     */

    /*
    @Autowired  not recommended, it is better to use constructor based dependency injection
    to improve readability, it is preferred to always use camel case to name attributes and methods
    */

    private TransferService transferService;

    // we should follow the best practices of rest APIs, for example:
    // rather than GET - /transfers/listDesTransfers, it is better to use GET - /transfers
    @GetMapping("") // to stick with the best practices of rest APIs, it is better to use this path instead
    public ResponseEntity<List<Transfer>> loadAllTransfers() { // it is better to use meaningful names for methods
        // it is bad practice to have application logic in the controller,
        // the task of controllers is to get requests and return responses, logic should be kept in services
        return new ResponseEntity<>(
                // using ResponseEntities is better to provide more information, like the status code, and hateoas.
                transferService.loadAllTransfers(),
                HttpStatus.OK
        );
    }

    /*
        It is more practical to make controllers for Accounts and Users,
        And it does not make any sense to have some endpoint like GET - /transfers/lister_utilisateurs
     */

    @PostMapping("")
    public ResponseEntity<Void> createTransaction(
            @RequestBody CreateTransferRequestDto transferDto
    ) {
        transferService.createTransfer(transferDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

}
