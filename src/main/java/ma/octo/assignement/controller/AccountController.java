package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.service.serviceImpl.AccountServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// this controller will deal with all the request concerning accounts
@RestController
@RequestMapping("/accounts")
@AllArgsConstructor // to deal with dependency injection
public class AccountController {

    private AccountServiceImpl accountService;

    @GetMapping("")
    public ResponseEntity<List<Account>> loadAllAccounts() {
        return new ResponseEntity<>(
                accountService.loadAllAccounts(),
                HttpStatus.OK
        );
    }
}
