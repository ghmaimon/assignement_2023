package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.model.Deposit;
import ma.octo.assignement.service.DepositService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposits")
@AllArgsConstructor
class DepositController {

    private DepositService depositService;

    @GetMapping("")
    public ResponseEntity<List<Deposit>> loadAllTransfers() {
        return new ResponseEntity<>(
                depositService.loadAllDeposits(),
                HttpStatus.OK
        );
    }


    @PostMapping("")
    public ResponseEntity<Void> createTransaction(
            @RequestBody CreateDepositRequestDto depositDto
    ) {
        depositService.createDeposit(depositDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

}
