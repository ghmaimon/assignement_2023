package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.model.User;
import ma.octo.assignement.service.serviceImpl.UserServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// this controller will deal with all the request concerning users

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {

    private UserServiceImpl userService;

    @GetMapping("")
    public ResponseEntity<List<User>> loadAllUsers() {
        return new ResponseEntity<>(
                userService.loadAllUsers(),
                HttpStatus.OK
        );
    }
}
