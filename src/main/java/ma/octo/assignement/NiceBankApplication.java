package ma.octo.assignement;

import lombok.AllArgsConstructor;
import ma.octo.assignement.model.Account;
import ma.octo.assignement.model.User;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
@AllArgsConstructor
public class NiceBankApplication implements CommandLineRunner {
	private AccountRepository compteRepository;
	private UserRepository utilisateurRepository;
	private TransferRepository transferRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		User utilisateur1 = new User();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");

		utilisateurRepository.save(utilisateur1);


		User utilisateur2 = new User();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		utilisateurRepository.save(utilisateur2);

		Account compte1 = new Account();
		compte1.setNumAccount("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setBalance(BigDecimal.valueOf(200000L));
		compte1.setUser(utilisateur1);

		compteRepository.save(compte1);

		Account compte2 = new Account();
		compte2.setNumAccount("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setBalance(BigDecimal.valueOf(140000L));
		compte2.setUser(utilisateur2);

		compteRepository.save(compte2);

		Transfer v = new Transfer();
		v.setAmount(BigDecimal.TEN);
		v.setAccountDebtor(compte2);
		v.setAccountCreditor(compte1);
		v.setDate(new Date());
		v.setReason("Assignment 2021");

		transferRepository.save(v);
	}
}
