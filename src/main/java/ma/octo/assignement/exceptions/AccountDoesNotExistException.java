package ma.octo.assignement.exceptions;

public class AccountDoesNotExistException extends NiceBankException {

  public AccountDoesNotExistException(String message) {
    super("Account not found: " + message);
  }
}
