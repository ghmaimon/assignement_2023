package ma.octo.assignement.exceptions;

public class InsufficientBalanceException extends NiceBankException {

  public InsufficientBalanceException(String message) {
    super("Insufficient balance: " + message);
  }
}
