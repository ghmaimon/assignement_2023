package ma.octo.assignement.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NiceBankException extends RuntimeException{

    private static final long serialVersionUID = 1L;
    public NiceBankException(String message) {
        super(message);
        log.error(message);
    }
}
