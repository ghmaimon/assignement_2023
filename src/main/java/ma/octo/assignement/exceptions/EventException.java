package ma.octo.assignement.exceptions;

public class EventException extends NiceBankException {
  public EventException(String message) {
    super("Event Exception: " + message);
  }
}
