package ma.octo.assignement.enumeration;

import lombok.Getter;
import lombok.Setter;

public enum AccountType {

    DEBTOR("Debtor"),
    CREDITOR("Creditor");

    private String type;

    AccountType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
