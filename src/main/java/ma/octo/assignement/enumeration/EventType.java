package ma.octo.assignement.enumeration;

public enum EventType {

    TRANSFER("Transfer"),
    DEPOSIT("Deposit");

    private String type;

    EventType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
