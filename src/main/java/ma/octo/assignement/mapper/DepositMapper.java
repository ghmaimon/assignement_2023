package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.model.Deposit;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DepositMapper {
    Deposit createDepositRequestDtoToDepositMapper(CreateDepositRequestDto dto);
}
