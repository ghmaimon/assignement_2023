package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.CreateDepositRequestDto;
import ma.octo.assignement.model.Deposit;
import ma.octo.assignement.model.Transfer;
import ma.octo.assignement.dto.CreateTransferRequestDto;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TransferMapper {
    Transfer createTransferRequestDtoToTransferMapper(CreateTransferRequestDto dto);
}
