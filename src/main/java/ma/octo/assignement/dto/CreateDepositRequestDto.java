package ma.octo.assignement.dto;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class CreateDepositRequestDto {
  private String fullNameCreditor;
  private String numAccountDebtor;
  private String reason;
  private BigDecimal amount;
  private Date date;
}
