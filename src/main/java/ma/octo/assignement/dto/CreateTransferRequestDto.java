package ma.octo.assignement.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class CreateTransferRequestDto {
  private String numAccountCreditor;
  private String numAccountDebtor;
  private String reason;
  private BigDecimal amount;
  private Date date;
}
