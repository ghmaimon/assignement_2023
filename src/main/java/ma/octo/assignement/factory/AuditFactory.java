package ma.octo.assignement.factory;

import ma.octo.assignement.model.Audit;
import ma.octo.assignement.model.Event;


public abstract class AuditFactory {

    protected StringBuilder auditMsgBuilder;

    public AuditFactory() {
        this.auditMsgBuilder = new StringBuilder();
    }

    public Audit performAuditOperation(Event event) {

        Audit audit = createAudit(event);

        auditMsgBuilder.delete(0, auditMsgBuilder.length());
        auditMsgBuilder.append(" to ");
        auditMsgBuilder.append(event.getAccountDebtor().getNumAccount());
        auditMsgBuilder.append(" of an amount ");
        auditMsgBuilder.append(event.getAmount());

        String auditMsg = auditMsgBuilder.toString();
        audit.setMessage(auditMsg);

        return audit;
    }

    protected abstract Audit createAudit(Event event);
}
