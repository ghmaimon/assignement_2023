package ma.octo.assignement.factory;

import ma.octo.assignement.model.Audit;
import ma.octo.assignement.model.AuditDeposit;
import ma.octo.assignement.model.Event;
import ma.octo.assignement.model.Deposit;
import org.springframework.stereotype.Component;

@Component
public class DepositAuditFactory extends AuditFactory{

    @Override
    protected Audit createAudit(Event event) {
        Audit audit = new AuditDeposit();
        auditMsgBuilder.append("Deposit from ");
        auditMsgBuilder.append(((Deposit)event).getFullNameCreditor());
        return audit;
    }
}
