package ma.octo.assignement.factory;

import ma.octo.assignement.model.*;
import org.springframework.stereotype.Component;

@Component
public class TransferAuditFactory extends AuditFactory{
    @Override
    protected Audit createAudit(Event event) {
        Audit audit = new AuditTransfer();
        auditMsgBuilder.append("Transfer from ");
        auditMsgBuilder.append(((Transfer)event).getAccountCreditor().getNumAccount());
        return audit;
    }
}
