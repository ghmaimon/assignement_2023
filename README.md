# Project Architecture
To facilitate the process of development (maintainability, readability...), I will work with this architecture:<br>

- com
  - ma
    - octo
      - assignment
        - NiceBankApplication.java
        - model
        - controller
        - controllerAdvice
        - config
        - dto
        - enumeration
        - service
        - exceptions
        - mapper
        - repository
        - util
        - factory